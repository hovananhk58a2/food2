<%@page import="java.security.MessageDigest"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@page import="com.mysql.jdbc.DatabaseMetaData"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Loading ...</title>
</head>
<body>
	<%
		request.setCharacterEncoding("UTF-8");
		DBConnect connect = new DBConnect();
		String table = "user";
		String newPassWord = request.getParameter("newPassword");

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(newPassWord.getBytes());

		byte byteData[] = md.digest();
		//convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		newPassWord = sb.toString();


		JSONObject json = new JSONObject(session.getAttribute("user").toString());
		String oldName = json.getString("name");
		String oldPassWord = json.getString("passWord");
		String oldGender = json.getString("gender");
		String oldUrlPicture = json.getString("urlPicture");
		String oldEmail = json.getString("email");
		String oldType = json.getString("type");
		String oldIdoftype = json.getString("idOfType");
		String id = "";

		String sql = "SELECT * FROM " + table + " WHERE name='" + oldName + "' AND urlPicture ='" + oldUrlPicture
				+ "' AND gender ='" + oldGender + "' AND email ='" + oldEmail + "'AND passWord ='" + oldPassWord + "'";
		ResultSet rs = connect.getRs(sql);
		try {
			while (rs.next()) {
				id = rs.getString("id");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String command = " UPDATE " + table + " SET passWord='" + newPassWord + "' WHERE id=" + id;
			out.print(command);
			connect.executeUpdate(command);
		} catch (Exception e) {
			out.print(e + "error");
		}

		
		User user = new User(0, oldName, newPassWord,oldEmail, oldGender, oldUrlPicture, oldType, oldIdoftype);
		JSONObject jsonToCookie = new JSONObject(user);

		session.setAttribute("user", jsonToCookie);
		Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
		userCookie.setMaxAge(60 * 60 * 24 * 30);
		userCookie.setPath("/");
		response.addCookie(userCookie);
		
		response.sendRedirect("userDetail.jsp");
	%>
</body>
</html>