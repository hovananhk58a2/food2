<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
<style type="text/css">
* {
	font-family: 'Open Sans', sans-serif;
}

.well {
	margin-top: -20px;
	background-color: #007FBD;
	border: 2px solid #0077B2;
	text-align: center;
	cursor: pointer;
	font-size: 25px;
	padding: 15px;
	border-radius: 0px !important;
}

.well:hover {
	margin-top: -20px;
	background-color: #0077B2;
	border: 2px solid #0077B2;
	text-align: center;
	cursor: pointer;
	font-size: 25px;
	padding: 15px;
	border-radius: 0px !important;
	border-bottom: 2px solid rgba(97, 203, 255, 0.65);
}

body {
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	font-size: 14px;
	line-height: 1.42857143;
	color: #fff;
	background-color: #F1F1F1;
}

.bg_blur {
	height: 300px;
	background-size: cover;
}

.follow_btn {
	text-decoration: none;
	position: absolute;
	left: 35%;
	top: 42.5%;
	width: 35%;
	height: 15%;
	background-color: #007FBE;
	padding: 10px;
	padding-top: 6px;
	color: #fff;
	text-align: center;
	font-size: 20px;
	border: 4px solid #007FBE;
}

.follow_btn:hover {
	text-decoration: none;
	position: absolute;
	left: 35%;
	top: 42.5%;
	width: 35%;
	height: 15%;
	background-color: #007FBE;
	padding: 10px;
	padding-top: 6px;
	color: #fff;
	text-align: center;
	font-size: 20px;
	border: 4px solid rgba(255, 255, 255, 0.8);
}

.header {
	color: #808080;
	margin-left: 10%;
	margin-top: 70px;
}

.picture {
	height: 150px;
	width: 150px;
	position: absolute;
	top: 75px;
	left: -75px;
}

.picture_mob {
	position: absolute;
	width: 35%;
	left: 35%;
	bottom: 70%;
}

.btn-style {
	color: #fff;
	background-color: #007FBE;
	border-color: #adadad;
	width: 33.3%;
}

.btn-style:hover {
	color: #333;
	background-color: #3D5DE0;
	border-color: #adadad;
	width: 33.3%;
}

@media ( max-width : 767px) {
	.header {
		text-align: center;
	}
	.nav {
		margin-top: 30px;
	}
}
</style>
<%@include file="style.jsp"%>
</head>
<body>
	<%
		String foodName = "";
		if (request.getParameter("name") != null) {
			foodName = request.getParameter("name");
		}
		String foodUrl = "";
		if (request.getParameter("url") != null) {
			foodUrl = request.getParameter("url");
		}
	%>
	<%@include file="header.jsp"%>
	<div class="container">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans'
			rel='stylesheet' type='text/css'>
		<link
			href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
			rel="stylesheet">

		<div class="container" style="margin-top: 20px; margin-bottom: 20px;">
			<div class="row panel">
				<div class="col-md-4 bg_blur"
					style=" background-image:url('<%=foodUrl%>');"></div>
				<div class="col-md-8  col-xs-12">
					<img src="http://lorempixel.com/output/people-q-c-100-100-1.jpg"
						class="img-thumbnail visible-xs picture_mob" />
					<div class="header">
						<h1><%=foodName%></h1>
						<h4>[user đăng]</h4>
						<span>[Phần miêu tả] ngon ngon ngon ngon ngon ngon ngon
							ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon
							ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon
							ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ngon ..."</span>
					</div>
				</div>
			</div>

			<div class="row nav">
				<div class="col-md-4"></div>
				<div class="col-md-8 col-xs-12" style="margin: 0px; padding: 0px;">
					<div class="col-md-4 col-xs-4 well">
						<i class="fa fa-weixin fa-lg"></i> 16
					</div>
					<div class="col-md-4 col-xs-4 well">
						<i class="fa fa-heart-o fa-lg"></i> 14
					</div>
					<div class="col-md-4 col-xs-4 well">
						<i class="glyphicon glyphicon-pushpin fa-lg"></i> 26
					</div>
				</div>
			</div>
		</div>

	</div>
	<%@include file="footer.jsp"%>
</body>
</html>