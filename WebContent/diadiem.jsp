<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@include file="style.jsp"%>
</head>
<body>
	<%@include file="header.jsp"%>
	<div class="container">
		<h1>
			<b>Bản đồ thực đơn</b><small> - Gần bạn nên ăn gì đây...</small>
		</h1>
	</div>

	<!-- jQuery -->
	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

	<div id="map" style="width: 100%; height: 500px;"></div>
	<script type="text/javascript" src="JS/ggmap/inimap.js">
		
	</script>

	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAP5bT16j_CS02Mmyf_1hYyw6fEd9v3PI&libraries=places&callback=initMap"
		async defer></script>
	<footer>
		<jsp:include page="footer.jsp"></jsp:include>
	</footer>
</body>
</html>