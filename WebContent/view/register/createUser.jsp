<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DatabaseMetaData"%>
<%@page import="java.security.MessageDigest"%>
<%@page import="com.food.db.DBConnect"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.food.user.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html">
<title>Loading ...</title>
</head>
<body>
	<%
		String email = request.getParameter("emailResgiter");

		DBConnect connect = new DBConnect();
		DatabaseMetaData dbm = (DatabaseMetaData) connect.getCon().getMetaData();

		String table = "user";
		ResultSet tables = dbm.getTables(null, null, table, null);

		String passWord = request.getParameter("passWord");

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(passWord.getBytes());

		byte byteData[] = md.digest();
		//convert the byte to hex format method 1
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		passWord = sb.toString();

		String name = request.getParameter("name");

		String urlPicture = "eset/image/avatar/default_avatar.jpg";

		String gender = request.getParameter("gender");
		
		String type = "normal";
		
		String idOfType = "";

		User user = new User(0, name, passWord,email, gender, urlPicture, type, idOfType);

		// Neu email da dang ki, thong bao cho nguoi dung va chuyen toi trang dang nhap

		if (tables.next()) {
			// Table exists 

			int checkExist = 0;
			String sql = "SELECT * FROM " + table + " WHERE email= '" + email + "'";
			ResultSet rs = connect.getRs(sql);
			try {

				while (rs.next()) {
					checkExist = 1;
				}

				if (checkExist == 1) {
					// Email da duoc dang ki

					response.sendRedirect("../login/normal/index.jsp?registered=" + email);
					return;

				} else {

					String command = "INSERT INTO " + table
							+ " ( name , passWord, email , gender , type , idoftype, urlPicture) VALUES ( '" + name
							+ "','" + passWord + "','" + email + "','" + gender + "','" + "normal" + "','" + ""
							+ "','" + urlPicture + "')";

					connect.executeUpdate(command);

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//out.println(user.toJsonString());

		} else {
			// Table does not exist
			try {
				String command = "CREATE TABLE " + table
						+ " (id int(225) NOT NULL AUTO_INCREMENT, name text(1000),passWord varchar(1000), email varchar(1000), gender varchar(1000), urlPicture varchar(5000), type varchar(1000), idoftype varchar(1000), PRIMARY KEY (id) )";
				connect.executeUpdate(command);

			} catch (Exception e) {

			}

			String command = "INSERT INTO " + table
					+ " ( name , passWord, email , gender , type , idoftype, urlPicture) VALUES ( '" + name + "','"
					+ passWord + "','" + email + "','" + gender + "','" + "normal" + "','" + "" + "','" + urlPicture
					+ "')";

			connect.executeUpdate(command);

		}

		// Cooki session
		JSONObject jsonToCookie = new JSONObject(user);

		session.setAttribute("user", jsonToCookie);
		Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
		userCookie.setMaxAge(60 * 60 * 24 * 30);
		userCookie.setPath("/");
		response.addCookie(userCookie);

		response.sendRedirect("../../main.jsp");
	%>
</body>
</html>