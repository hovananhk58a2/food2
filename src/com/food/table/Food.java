package com.food.table;
import java.util.ArrayList;

public class Food {
	private int foodCode;
	private String foodName;
	private String foodAddress;
	private ArrayList<Integer> foodclass;
	private String imageUrl;
	private int userId;
	private ArrayList<Integer> likes;
	private ArrayList<Integer> pins;
	private ArrayList<Comment> comments;
	
	public Food() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Food(int foodCode, String foodName, String imageUrl, ArrayList<Integer> foodclass) {
		super();
		this.foodCode = foodCode;
		this.foodName = foodName;
		this.imageUrl = imageUrl;
		this.foodclass = foodclass;
	}

	public int getFoodCode() {
		return foodCode;
	}

	public void setFoodCode(int foodCode) {
		this.foodCode = foodCode;
	}

	public String getFoodName() {
		return foodName;
	}

	public void setFoodName(String foodName) {
		this.foodName = foodName;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ArrayList<Integer> getFoodclass() {
		return foodclass;
	}

	public void setFoodclass(ArrayList<Integer> foodclass) {
		this.foodclass = foodclass;
	}

	public String getFoodAddress() {
		return foodAddress;
	}

	public void setFoodAddress(String foodAddress) {
		this.foodAddress = foodAddress;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList<Integer> getLikes() {
		return likes;
	}

	public void setLikes(ArrayList<Integer> likes) {
		this.likes = likes;
	}

	public ArrayList<Integer> getPins() {
		return pins;
	}

	public void setPins(ArrayList<Integer> pins) {
		this.pins = pins;
	}

	public ArrayList<Comment> getComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Food [foodCode=" + foodCode + ", foodName=" + foodName + ", imageUrl=" + imageUrl + ", foodclass="
				+ foodclass + "]";
	}

}
