<%@page import="java.io.Console"%>
<%@page import="java.security.MessageDigest"%>
<%@page import="com.food.db.DBConnect"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="org.json.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>
	<%
		request.setCharacterEncoding("UTF-8");
		if (session.getAttribute("user") == null) {
			response.sendRedirect("../login/normal/index.jsp");
		} else {
			JSONObject json = new JSONObject(session.getAttribute("user").toString());
			String name = json.getString("name");

			String urlPicture = "";
			String email = "";
			String passWordFromSession = "";
			String gender = "";

			try {
				passWordFromSession = json.getString("passWord");

			} catch (Exception e) {
			}

			try {
				String s = json.getString("urlPicture");
				if (s.startsWith("eset")) {
					urlPicture = "../../" + s;
				} else {
					urlPicture = s;
				}

			} catch (Exception e) {
			}

			try {
				email = json.getString("email");
			} catch (Exception e) {
			}

			try {
				gender = json.getString("gender");
			} catch (Exception e) {
			}

			out.println(name);
	%>
</title>
<link rel="stylesheet" href="./styleTabInfor/aui.min.css" media="all">
<link rel="stylesheet" href="./styleTabInfor/aui-experimental.min.css"
	media="all">
<link rel="stylesheet" href="./styleTabInfor/base.less.css" media="all">
<link rel="stylesheet" href="./styleTabInfor/logged-in.less.css"
	media="all">
<link rel="stylesheet" href="./styleTabInfor/profile.less.css"
	media="all">
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<meta name="description" content="Developed By M Abdur Rokib Promy">
<meta name="keywords"
	content="Admin, Bootstrap 3, Template, Theme, Responsive">
<!-- bootstrap 3.0.2 -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="css/morris/morris.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet"
	type="text/css" />
<!-- Date Picker -->
<link href="css/datepicker/datepicker3.css" rel="stylesheet"
	type="text/css" />
<!-- fullCalendar -->
<!-- <link href="css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
<!-- Daterange picker -->
<link href="css/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" type="text/css" />
<!-- iCheck for checkboxes and radio inputs -->
<link href="css/iCheck/all.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
<link href='http://fonts.googleapis.com/css?family=Lato'
	rel='stylesheet' type='text/css'>
<!-- Theme style -->
<link href="css/style.css" rel="stylesheet" type="text/css" />


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

<style type="text/css">
</style>
</head>

<body class="skin-black">
	<header>
		<jsp:include page="../../header.jsp"></jsp:include>
	</header>

	<div class="wrapper row-offcanvas row-offcanvas-left">

		<aside class="left-side sidebar-offcanvas">

			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<%out.println(urlPicture);%>" class="img-circle"
							alt="User Image" />
					</div>
					<div class="pull-left info">
						<p>
							Hello
							<%
							out.println(name);
						%>
						</p>
						<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
					</div>
				</div>
				<!-- search form -->
				<form action="#" method="get" class="sidebar-form">
					<div class="input-group">
						<input type="text" name="q" class="form-control"
							placeholder="Search..." /> <span class="input-group-btn">
							<button type='submit' name='seach' id='search-btn'
								class="btn btn-flat">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li><a class="tab" onclick="openTab(event,'timeLine')"
						style="background-color: #4c556c"> <i class="fa fa-dashboard"></i>
							<span>Bài đăng</span>
					</a></li>

					<li><a class="tab" onclick="openTab(event,'infor')"> <i
							class="fa fa-globe"></i> <span>Tài khoản</span>
					</a></li>

					<li><a class="tab" onclick="openTab(event,'photo')"> <i
							class="fa fa-glass"></i> <span>Liên hệ</span>
					</a></li>

				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<aside class="right-side">

			<!-- Main content -->

			<section class="content" id="infor" style="display: none">
				<div id="page">
					<div class="aui-page-panel">
						<div class="aui-page-panel-inner">
							<div class="aui-page-panel-nav aid-sidebar">
								<nav class="aui-navgroup aui-navgroup-vertical">
									<div class="aui-navgroup-inner">
										<div class="aui-nav-heading">
											<strong>Chung</strong>
										</div>
										<ul class="aui-nav">
											<li><a onclick="showContent('userInfor')">Thông tin</a></li>
										</ul>
										<div class="aui-nav-heading">
											<strong>Bảo mật</strong>
										</div>
										<ul class="aui-nav">
											<%
												if (passWordFromSession.length() > 1) {
											%><li><a onclick="showContent('changPass')">Thay đổi
													mật khẩu</a></li>
											<%
												}
											%>

											<li><a onclick="showContent('changEmail')">Thay đổi
													email</a></li>
										</ul>
										<div class="aui-nav-heading">
											<strong>Hóa đơn</strong>
										</div>
										<ul class="aui-nav">
											<li><a onclick="showContent('changEmail')">Chi tiết
													hóa đơn</a></li>
										</ul>
									</div>
								</nav>
							</div>
							<!-- .aui-page-panel-nav -->

							<section id="" class="aui-page-panel-content"
								style="display: none"></section>


							<section id="changPass" class="aui-page-panel-content"
								style="display: none">
								<h2>Thay đổi mật khẩu</h2>

								<form id="change-email-form" class="aui" method="post"
									action="updatePassWord.jsp">
									<div class="field-group">
										<label for="newEmail">Mật khẩu cũ</label><input required
											class="text" type="password" name="oldPassWord"
											id="old_passWord" value="Đang tìm cách sinh mã sha trong jsp">
									</div>

									<div class="field-group">
										<label for="newEmail">Mật khẩu mới</label><input required
											class="text" name="newPassword" type="password"
											name="password" id="password">
									</div>


									<div class="field-group">
										<label for="newEmail">Nhập lại mật khẩu</label><input required
											name="confirm_password" class="text" type="password"
											name="newEmail" id="confirm_password">
									</div>

									<div class="buttons-container">
										<div class="buttons">
											<input type="submit" class="aui-button aui-button-primary"
												value="Cập nhật">
										</div>
									</div>
								</form>
								<div id="change-email-pending"
									class="aui-message aui-message-warning warning"
									style="display: none;">
									<p class="title">
										<strong>Update pending verification</strong>
									</p>
									We just need you to check your email <strong></strong> and
									confirm your password to verify it's you and complete the
									update.
									<ul class="aui-nav-actions-list">
										<li><a
											href="https://id.atlassian.com/manage/change-email#"
											class="change-email-resend">Resend email</a></li>
										<li><a
											href="https://id.atlassian.com/manage/change-email#"
											class="change-email-undo">Undo email change</a></li>
									</ul>
								</div>
							</section>



							<section id="changEmail" class="aui-page-panel-content"
								style="display: none">
								<h2>Thay đổi email</h2>
								<p>
									Địa chỉ email hiện tại của bạn là <strong> <%
 	if (email.length() > 1) {
 			out.print(email);
 		}
 %>
									</strong>
								</p>
								<form id="change-email-form" class="aui"
									action="updateEmail.jsp" method="post">
									<div class="field-group">
										<label for="newEmail">Địa chỉ email mới</label><input required
											class="text" type="email" name="newEmail" id="newEmail">
									</div>
									<input class="hidden" type="hidden" name="csrfToken"
										id="csrfToken"
										value="ce7bd202b265a17b83bfcb580b58fb40dbd0775f">
									<div class="buttons-container">
										<div class="buttons">
											<input type="submit" class="aui-button aui-button-primary"
												value="Cập nhật">
										</div>
									</div>
								</form>
								<div id="change-email-pending"
									class="aui-message aui-message-warning warning"
									style="display: none;">
									<p class="title">
										<strong>Update pending verification</strong>
									</p>
									We just need you to check your email <strong></strong> and
									confirm your password to verify it's you and complete the
									update.
									<ul class="aui-nav-actions-list">
										<li><a
											href="https://id.atlassian.com/manage/change-email#"
											class="change-email-resend">Resend email</a></li>
										<li><a
											href="https://id.atlassian.com/manage/change-email#"
											class="change-email-undo">Undo email change</a></li>
									</ul>
								</div>
							</section>

							<section id="userInfor" class="aui-page-panel-content"
								style="display: block">
								<h2>Thông tin tài khoản</h2>
								<div class="aui-group account-settings">
									<div class="aui-item">
										<form id="account-settings-form" class="aui"
											action="updateUser.jsp" method="post">
											<div class="field-group">
												<label for="email">Email</label><span id="email"
													class="field-value"> <%
 	out.print(email);
 %>
												</span> <a onclick="showContent('changEmail')"
													class="account-settings-change-email">Thay đổi email</a>
											</div>
											<div class="field-group">
												<label for="fullName">Tên đầy đủ<span
													class="aui-icon icon-required"></span></label> <input class="text"
													type="text" name="fullName" id="fullName"
													value="<%out.print(name);%>">
											</div>

											<div class="field-group">
												<label for="timezone">Giới tính</label><select name="gender"
													class="select">
													<option class="genderSelect" value="Nam">Nam</option>
													<option class="genderSelect" value="Nữ">Nữ</option>
													<option class="genderSelect" value="Khác">Khác</option>
												</select>
												<div class="error" style="display: none">Hiện lỗi ở
													đây.</div>
											</div>
											<div class="buttons-container">
												<div class="buttons">
													<input type="submit" class="aui-button aui-button-primary"
														value="Cập nhật">
												</div>
											</div>
										</form>
									</div>

									<div class="aui-item avatar-panel">
										<div class="user-avatar-form">
											<span class="aui-avatar aui-avatar-xxxlarge"> <span
												class="aui-avatar-inner"> <img id="avatar"
													src="<%out.print(urlPicture);%>">
											</span> </span> <a onclick="changeAvatar();"
												class="aui-button aui-button-link avatar-picker-trigger"
												type="button" tabindex="0">Đổi avatar</a> <input
												id="changAvatar" type="file" value="Đổi avatar"
												class="aui-button aui-button-link avatar-picker-trigger"
												data-ng-disabled="disabled" style="display: none;">
										</div>
									</div>
								</div>
							</section>

							<!-- .aui-page-panel-content -->
						</div>
						<!-- .aui-page-panel-inner -->
					</div>
				</div>
			</section>

			<section class="content" id="photo" style="display: none"></section>

			<section class="content" id="timeLine" style="display: block">

				<div class="row" style="margin-bottom: 5px; margin-top: 50px">

					<div class="col-md-3">
						<div class="sm-st clearfix">
							<span class="sm-st-icon st-red"><i
								class="fa fa-check-square-o"></i></span>
							<div class="sm-st-info">
								<span>3200</span> Lượt xem
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="sm-st clearfix">
							<span class="sm-st-icon st-violet"><i
								class="fa fa-envelope-o"></i></span>
							<div class="sm-st-info">
								<span>2200</span> Tin nhắn
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="sm-st clearfix">
							<span class="sm-st-icon st-blue"><i class="fa fa-dollar"></i></span>
							<div class="sm-st-info">
								<span>100,320</span> Thu nhập
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="sm-st clearfix">
							<span class="sm-st-icon st-green"><i
								class="fa fa-paperclip"></i></span>
							<div class="sm-st-info">
								<span>4567</span> Tài liêu
							</div>
						</div>
					</div>
				</div>

				<!-- Main row -->
				<div class="row">

					<div class="col-md-8">
						<!--earning graph start-->
						<section class="panel">
							<header class="panel-heading"> Biều đồ </header>
							<div class="panel-body">
								<canvas id="linechart" width="600" height="330"></canvas>
							</div>
						</section>
						<!--earning graph end-->

					</div>
					<div class="col-lg-4">

						<!--chat start-->
						<section class="panel">
							<header class="panel-heading"> Thông báo </header>
							<div class="panel-body" id="noti-box">

								<div class="alert alert-block alert-danger">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Like !</strong> Hoàn Kì thích bài đăng của bạn.
								</div>
								<div class="alert alert-success">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Comment!</strong> Hồ Anh đã bình luận bài đăng của bạn.
								</div>
								<div class="alert alert-info">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Comment!</strong> Đặng Bình đã bình luận bài đăng của
									bạn.
								</div>
								<div class="alert alert-warning">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Share!</strong> Phạm lài đã chia sẻ bài đăng của bạn.
								</div>


								<div class="alert alert-block alert-danger">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Like!</strong> Chấp Dương thích hình ảnh của bạn.
								</div>
								<div class="alert alert-success">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Like!</strong> Chấp Dương thích hình ảnh của bạn.
								</div>
								<div class="alert alert-info">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Like!</strong> Chấp Dương thích hình ảnh của bạn.
								</div>
								<div class="alert alert-warning">
									<button data-dismiss="alert" class="close close-sm"
										type="button">
										<i class="fa fa-times"></i>
									</button>
									<strong>Like!</strong> Chấp Dương thích hình ảnh của bạn.
								</div>


							</div>
						</section>


					</div>


				</div>
				<div class="row">

					<div class="col-md-8">
						<section class="panel">
							<header class="panel-heading"> Tiến độ </header>
							<div class="panel-body table-responsive">
								<table class="table table-hover">
									<thead>
										<tr>
											<th>#</th>
											<th>Project</th>
											<th>Manager</th>
											<!-- <th>Client</th> -->
											<th>Deadline</th>
											<!-- <th>Price</th> -->
											<th>Status</th>
											<th>Progress</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>Facebook</td>
											<td>Mark</td>
											<!-- <td>Steve</td> -->
											<td>10/10/2014</td>
											<!-- <td>$1500</td> -->
											<td><span class="label label-danger">in progress</span></td>
											<td><span class="badge badge-info">50%</span></td>
										</tr>
										<tr>
											<td>2</td>
											<td>Twitter</td>
											<td>Evan</td>
											<!-- <td>Darren</td> -->
											<td>10/8/2014</td>
											<!-- <td>$1500</td> -->
											<td><span class="label label-success">completed</span></td>
											<td><span class="badge badge-success">100%</span></td>
										</tr>
										<tr>
											<td>3</td>
											<td>Google</td>
											<td>Larry</td>
											<!-- <td>Nick</td> -->
											<td>10/12/2014</td>
											<!-- <td>$2000</td> -->
											<td><span class="label label-warning">in progress</span></td>
											<td><span class="badge badge-warning">75%</span></td>
										</tr>
										<tr>
											<td>4</td>
											<td>LinkedIn</td>
											<td>Allen</td>
											<!-- <td>Rock</td> -->
											<td>10/01/2015</td>
											<!-- <td>$2000</td> -->
											<td><span class="label label-info">in progress</span></td>
											<td><span class="badge badge-info">65%</span></td>
										</tr>
										<tr>
											<td>5</td>
											<td>Tumblr</td>
											<td>David</td>
											<!-- <td>HHH</td> -->
											<td>01/11/2014</td>
											<!-- <td>$2000</td> -->
											<td><span class="label label-warning">in progress</span></td>
											<td><span class="badge badge-danger">95%</span></td>
										</tr>
										<tr>
											<td>6</td>
											<td>Tesla</td>
											<td>Musk</td>
											<!-- <td>HHH</td> -->
											<td>01/11/2014</td>
											<!-- <td>$2000</td> -->
											<td><span class="label label-info">in progress</span></td>
											<td><span class="badge badge-success">95%</span></td>
										</tr>
										<tr>
											<td>7</td>
											<td>Ghost</td>
											<td>XXX</td>
											<!-- <td>HHH</td> -->
											<td>01/11/2014</td>
											<!-- <td>$2000</td> -->
											<td><span class="label label-info">in progress</span></td>
											<td><span class="badge badge-success">95%</span></td>
										</tr>
									</tbody>
								</table>
							</div>
						</section>


					</div>
					<!--end col-6 -->
					<div class="col-md-4">
						<section class="panel">
							<header class="panel-heading"> Bảng tin</header>
							<div class="panel-body">
								<div class="twt-area">
									<form action="#" method="post">
										<textarea class="form-control" name="profile-tweet"
											placeholder="Chia sẻ với Ăn gì ... " rows="3"></textarea>

										<div class="clearfix">
											<button class="btn btn-sm btn-primary pull-right"
												type="submit">
												<i class="fa fa-twitter"></i> Đăng
											</button>
											<a class="btn btn-link btn-icon fa fa-location-arrow"
												data-original-title="Add Location" data-placement="bottom"
												data-toggle="tooltip" href="#"
												style="text-decoration: none;" title=""></a> <a
												class="btn btn-link btn-icon fa fa-camera"
												data-original-title="Add Photo" data-placement="bottom"
												data-toggle="tooltip" href="#"
												style="text-decoration: none;" title=""></a>
										</div>
									</form>
								</div>
								<ul class="media-list">
									<li class="media"><a href="#" class="pull-left"> <img
											src="img/26115.jpg" alt="Avatar" class="img-circle"
											width="64" height="64">
									</a>
										<div class="media-body">
											<span class="text-muted pull-right"> <small><em>30
														min ago</em></small>
											</span> <a href="page_ready_user_profile.php"> <strong>
													<%
														out.println(name);
													%>
											</strong>
											</a>
											<p>
												Mời ghé thăm <a
													href="https://www.facebook.com/AnhHo95?fref=ts">Hồ Anh</a>
												để ngắm vẻ đẹp trai của lão. <a href="#" class="text-danger">
													<strong> ! </strong>
												</a>
											</p>
										</div></li>
									<li class="media"><a href="#" class="pull-left"> <img
											src="img/26115.jpg" alt="Avatar" class="img-circle"
											width="64" height="64">
									</a>
										<div class="media-body">
											<span class="text-muted pull-right"> <small><em>30
														min ago</em></small>
											</span> <a href="page_ready_user_profile.php"> <strong>
													<%
														out.println(name);
													%>
											</strong>
											</a>
											<p>
												Mời ghé thăm <a
													href="https://www.facebook.com/profile.php?id=100005180071626&fref=ts">Chấp
													Dương</a> để xem độ dâm của lão.<a href="#" class="text-danger">
													<strong> ! </strong>
												</a>
											</p>
										</div></li>
								</ul>
							</div>
						</section>
					</div>

				</div>
				<div class="row">
					<div class="col-md-5">
						<div class="panel">
							<header class="panel-heading"> Teammates </header>

							<ul class="list-group teammates">
								<li class="list-group-item"><a href=""><img
										src="img/26115.jpg" width="50" height="50"></a> <span
									class="pull-right label label-danger inline m-t-15">Admin</span>
									<a href="">Damon Parker</a></li>
								<li class="list-group-item"><a href=""><img
										src="img/26115.jpg" width="50" height="50"></a> <span
									class="pull-right label label-info inline m-t-15">Member</span>
									<a href="">Joe Waston</a></li>
								<li class="list-group-item"><a href=""><img
										src="img/26115.jpg" width="50" height="50"></a> <span
									class="pull-right label label-warning inline m-t-15">Editor</span>
									<a href="">Jannie Dvis</a></li>
								<li class="list-group-item"><a href=""><img
										src="img/26115.jpg" width="50" height="50"></a> <span
									class="pull-right label label-warning inline m-t-15">Editor</span>
									<a href="">Emma Welson</a></li>
								<li class="list-group-item"><a href=""><img
										src="img/26115.jpg" width="50" height="50"></a> <span
									class="pull-right label label-success inline m-t-15">Subscriber</span>
									<a href="">Emma Welson</a></li>
							</ul>
							<div class="panel-footer bg-white">
								<!-- <span class="pull-right badge badge-info">32</span> -->
								<button class="btn btn-primary btn-addon btn-sm">
									<i class="fa fa-plus"></i> Add Teammate
								</button>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<section class="panel tasks-widget">
							<header class="panel-heading"> Todo list </header>
							<div class="panel-body">

								<div class="task-content">

									<ul class="task-list">
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey list-child" />
												<!-- <input type="checkbox" class="square-grey"/> -->
											</div>
											<div class="task-title">
												<span class="task-title-sp">Director is Modern
													Dashboard</span> <span class="label label-success">2 Days</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Fully Responsive And
													Bootstrap 3.0.2 Compatible</span> <span class="label label-danger">Done</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Latest Design Concept</span> <span
													class="label label-warning">Company</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Write well documentation
													for this theme</span> <span class="label label-primary">3
													Days</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Don't bother to download
													this Dashbord</span> <span class="label label-inverse">Now</span>
												<div class="pull-right">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Give feedback for the
													template</span> <span class="label label-success">2 Days</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>
										<li>
											<div class="task-checkbox">
												<!-- <input type="checkbox" class="list-child" value=""  /> -->
												<input type="checkbox" class="flat-grey" />
											</div>
											<div class="task-title">
												<span class="task-title-sp">Tell your friends about
													this admin template</span> <span class="label label-danger">Now</span>
												<div class="pull-right hidden-phone">
													<button class="btn btn-default btn-xs">
														<i class="fa fa-check"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-pencil"></i>
													</button>
													<button class="btn btn-default btn-xs">
														<i class="fa fa-times"></i>
													</button>
												</div>
											</div>
										</li>

									</ul>
								</div>

								<div class=" add-task-row">
									<a class="btn btn-success btn-sm pull-left" href="#">Add
										New Tasks</a> <a class="btn btn-default btn-sm pull-right"
										href="#">See All Tasks</a>
								</div>
							</div>
						</section>
					</div>
				</div>
				<!-- row end -->
			</section>
			<!-- /.content -->

		</aside>
		<!-- /.right-side -->

	</div>
	<!-- ./wrapper -->


	<!-- jQuery 2.0.2 -->
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="js/jquery.min.js" type="text/javascript"></script>

	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- daterangepicker -->
	<script src="js/plugins/daterangepicker/daterangepicker.js"
		type="text/javascript"></script>

	<script src="js/plugins/chart.js" type="text/javascript"></script>

	<!-- datepicker
<script src="js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
	<!-- Bootstrap WYSIHTML5
<script src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
	<!-- iCheck -->
	<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
	<!-- calendar -->
	<script src="js/plugins/fullcalendar/fullcalendar.js"
		type="text/javascript"></script>

	<!-- Director App -->
	<script src="js/Director/app.js" type="text/javascript"></script>

	<!-- Director dashboard demo (This is only for demo purposes) -->
	<script src="js/Director/dashboard.js" type="text/javascript"></script>

	<!-- Director for demo purposes -->
	<script type="text/javascript">
		$('input').on('ifChecked', function(event) {
			// var element = $(this).parent().find('input:checkbox:first');
			// element.parent().parent().parent().addClass('highlight');
			$(this).parents('li').addClass("task-done");
			console.log('ok');
		});
		$('input').on('ifUnchecked', function(event) {
			// var element = $(this).parent().find('input:checkbox:first');
			// element.parent().parent().parent().removeClass('highlight');
			$(this).parents('li').removeClass("task-done");
			console.log('not');
		});
	</script>
	<script>
		$('#noti-box').slimScroll({
			height : '400px',
			size : '5px',
			BorderRadius : '5px'
		});

		$('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey')
				.iCheck({
					checkboxClass : 'icheckbox_flat-grey',
					radioClass : 'iradio_flat-grey'
				});
	</script>
	<script type="text/javascript">
		$(function() {
			"use strict";
			//BAR CHART
			var data = {
				labels : [ "January", "February", "March", "April", "May",
						"June", "July" ],
				datasets : [ {
					label : "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)",
					data : [ 65, 59, 80, 81, 56, 55, 40 ]
				}, {
					label : "My Second dataset",
					fillColor : "rgba(151,187,205,0.2)",
					strokeColor : "rgba(151,187,205,1)",
					pointColor : "rgba(151,187,205,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(151,187,205,1)",
					data : [ 28, 48, 40, 19, 86, 27, 90 ]
				} ]
			};
			new Chart(document.getElementById("linechart").getContext("2d"))
					.Line(data, {
						responsive : true,
						maintainAspectRatio : false,
					});

		});
		// Chart.defaults.global.responsive = true;

		function openTab(evt, tabName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("content");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			document.getElementById(tabName).style.display = "block";

			tablinks = document.getElementsByClassName("tab");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].style.background = "#39435c";
			}
			evt.currentTarget.style.background = "#4c556c";
		}

		function showContent(id) {
			tabcontent = document
					.getElementsByClassName("aui-page-panel-content");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}

			document.getElementById(id).style.display = "block";

		}

		selectGender();
		function selectGender() {
			genderSelect = document.getElementsByClassName("genderSelect");
			for (i = 0; i < genderSelect.length; i++) {
				if (genderSelect[i].value == "<%out.print(gender);%>") {
					genderSelect[i].selected = true;
				}
			}
		}

		function changeAvatar() {
			document.getElementById('changAvatar').click();
			document.getElementById("avatar").src = document
					.getElementById("changAvatar").value;
		}

		var password = document.getElementById("password"), confirm_password = document
				.getElementById("confirm_password"), old_password = document
				.getElementById("old_passWord");

		function validatePassword() {
			
			// Sha 256 jsp ???
			
			// js -> cookie
			document.cookie = "old_passWord_inpt=" + old_password.value;
			
			
			// jsp lay cookie, sua , luu
			<%Cookie cookie = null;
				Cookie[] cookies = null;

				cookies = request.getCookies();
				if (cookies != null) {
					for (int i = 0; i < cookies.length; i++) {
						cookie = cookies[i];
						if (cookie.getName().equals("old_passWord_inpt")) {
							String pass_in_cookie = "123";
							pass_in_cookie = cookie.getValue();

							System.out.println(pass_in_cookie);

							MessageDigest md = MessageDigest.getInstance("SHA-256");
							md.update(pass_in_cookie.getBytes());

							byte byteData[] = md.digest();
							//convert the byte to hex format method 1
							StringBuffer sb = new StringBuffer();
							for (int j = 0; j < byteData.length; j++) {
								sb.append(Integer.toString((byteData[j] & 0xff) + 0x100, 16).substring(1));
							}
							pass_in_cookie = sb.toString();

							Cookie userCookie = new Cookie("old_passWord_inpt2", pass_in_cookie);
							userCookie.setMaxAge(60 * 60 * 24 * 30);
							userCookie.setPath("/");
							response.addCookie(userCookie);
							break;
						}
					}
				}%>
			
			// js lay pass tu cookie
			var old_passWord_inpt_in_Cookie = "";
			var allcookies = document.cookie;
			cookiearray = allcookies.split(';');
			for (var i = 0; i < cookiearray.length; i++) {
				if (cookiearray[i].split('=')[0] == "old_passWord_inpt2") {
					old_passWord_inpt_in_Cookie = cookiearray[i].split('=')[1];
				}
			}
			<%--
			if (old_passWord_inpt_in_Cookie != "<%=passWordFromSession%>") {
				old_password.setCustomValidity("Bạn nhập sai mật khẩu");
			} else {
				old_password.setCustomValidity('');
			}
			--%>
			if (password.value != confirm_password.value) {
				confirm_password.setCustomValidity("Mật khẩu không giống");
			} else {
				confirm_password.setCustomValidity('');
			}
		}

		old_password.onkeyup = validatePassword;
		password.onchange = validatePassword;
		confirm_password.onkeyup = validatePassword;
	</script>
</body>
</html>
<%
	}
%>