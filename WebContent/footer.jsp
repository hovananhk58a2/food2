<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
	<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
</head>
<body>
	<footer class="footer-distributed">

	<div class="footer-left">

		<h3>
			HUS <span>University</span>
		</h3>

		<p class="footer-links">
			<a href="#">Home</a> · <a href="#">Blog</a> · <a href="#">Pricing</a>
			· <a href="#">About</a> · <a href="#">Faq</a> · <a href="#">Contact</a>
		</p>

		<p class="footer-company-name">Phát triển &copy; 2016</p>
	</div>

	<div class="footer-center">

		<div>
			<i class="fa fa-map-marker"></i>
			<p>
				<span>322 Nguyễn trãi</span> Thanh Xuân, Hà Nội
			</p>
		</div>

		<div>
			<i class="fa fa-phone"></i>
			<p>+84 964 279 609</p>
		</div>

		<div>
			<i class="fa fa-envelope"></i>
			<p>
				<a href="mailto:Khoahoctunhien_k58Cis@hus.edu.vn">Khoahoctunhien_k58Cis@hus.edu.vn</a>
			</p>
		</div>

	</div>

	<div class="footer-right">

		<p class="footer-company-about">
			<span>Thông tin</span> Trang web phục vụ đăng bán và tìm kiếm đồ ăn thức uống mọi lúc mọi nơi, hãy cho chúng tôi biết về ý kiến của bạn.
		</p>

		<div class="footer-icons">

			<a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i
				class="fa fa-twitter"></i></a> <a href="#"><i class="fa fa-linkedin"></i></a>
			<a href="#"><i class="fa fa-github"></i></a>

		</div>

	</div>
	</footer>
</body>
</html>