package com.food.query;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import com.food.db.DBConnect;
import com.food.table.Food;
import com.food.table.FoodClass;

public class FoodQuery {

	public FoodQuery() {
	}

	public ArrayList<Food> getAll() {
		ArrayList<Food> foods = new ArrayList<Food>();
		DBConnect connect = new DBConnect();
		String sql = "select * from food";
		ResultSet rs = connect.getRs(sql);
		try {
			while (rs.next()) {
				Food food = new Food();
				food.setFoodCode(rs.getInt("foodcode"));
				food.setFoodName(rs.getString("foodname"));
				try {
					JSONArray array = new JSONArray(rs.getString("foodclass"));
					food.setFoodclass(new ArrayList<Integer>());
					for (int i = 0; i < array.length(); i++) {
						food.getFoodclass().add(Integer.parseInt("" + array.get(i)));
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				food.setImageUrl(rs.getString("imageurl"));
				foods.add(food);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return foods;
	}

	public ArrayList<FoodClass> getFoodClass() {
		ArrayList<FoodClass> foodClasses = new ArrayList<FoodClass>();
		DBConnect connect = new DBConnect();
		String sql = "select * from foodclass";
		ResultSet rs = connect.getRs(sql);
		try {
			while (rs.next()) {
				FoodClass foodClass = new FoodClass();
				foodClass.setClassCode(rs.getInt("classcode"));
				foodClass.setClassName(rs.getString("classname"));
				foodClasses.add(foodClass);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connect.closeCon();
		return foodClasses;
	}

	public ArrayList<Food> getFoodsByCategory(int fclass) {
		ArrayList<Food> foods = getAll();
		if (fclass == 1)
			return foods;
		ArrayList<Food> rt = new ArrayList<>();
		for (int i = 0; i < foods.size(); i++) {
			if (foods.get(i).getFoodclass().contains(fclass)) {
				rt.add(foods.get(i));
			}
		}
		return rt;
	}
}
