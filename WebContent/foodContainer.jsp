<%@page import="com.food.table.Food"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.query.FoodQuery"%>
<%@page import="com.food.db.DBConnect"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	int classCode = 1;
	String className = "Tất cả";
	if (request.getParameter("fclass") != null) {
		classCode = Integer.parseInt(request.getParameter("fclass"));
	}
	if (request.getParameter("name") != null) {
		className = request.getParameter("name");
	}
%>
<script>
	$("#infoContain").html("");
</script>
<div>
	<h3 id="love" style="text-align: center;"><%=className%></h3>
</div>

<%
	FoodQuery foodQuery = new FoodQuery();
	ArrayList<Food> list = foodQuery.getFoodsByCategory(classCode);
	for (int i = 0; i < list.size(); i++) {
%>
<div class="img">
	<div class="hovereffect">
		<a
			href="foodDetail.jsp?name=<%=list.get(i).getFoodName()%>&url=<%=list.get(i).getImageUrl()%>"><img
			id="foodimage" src=<%=list.get(i).getImageUrl()%> alt="Tên món"></a>
		<div class="overlay">

			<p class="icon-links">
				<a href="#"><img alt="Ghim" src="icon/push-pin.png"
					style="width: 24px; height: 24px;"> </a> <a href="#"> <img
					alt="Yêu thích" src="icon/like.png"
					style="width: 24px; height: 24px;"></a> <a href="#"><img
					alt="Chat" src="icon/chat.png"
					style="width: 24px; height: 24px; margin-right: 40px;"> </a>
			</p>
		</div>
	</div>
	<div class="desc"><%=list.get(i).getFoodName()%></div>
</div>

<%
	}
%>
