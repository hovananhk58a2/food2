<%@page import="com.mysql.jdbc.DatabaseMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.food.db.DBConnect"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mysql.jdbc.jdbc2.optional.SuspendableXAConnection"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.HttpURLConnection"%>
<%@page import="java.io.DataInputStream"%>
<%@page import="java.io.DataOutputStream"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@ page import="com.food.user.*"%>
<%@include file="../../../config.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	response.setContentType("text/html;charset=UTF-8");
	request.setCharacterEncoding("utf-8");
	String code = request.getParameter("code");

	String httpsURL = "https://www.googleapis.com/oauth2/v4/token";

	StringBuilder q = new StringBuilder();
	q.append("client_id=" + URLEncoder.encode(google_client_id, "UTF-8"));
	q.append("&redirect_uri=" + URLEncoder.encode(google_redirect_uri, "UTF-8"));
	q.append("&client_secret=" + URLEncoder.encode(google_client_secret, "UTF-8"));
	q.append("&grant_type=" + URLEncoder.encode("authorization_code", "UTF-8"));
	q.append("&code=" + URLEncoder.encode(code, "UTF-8"));

	String query = q.toString();
	URL myurl = new URL(httpsURL);
	HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();
	con.setRequestMethod("POST");
	con.setRequestProperty("Content-length", String.valueOf(query.length()));
	con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0;Windows98;DigExt)");
	con.setDoOutput(true);
	con.setDoInput(true);

	DataOutputStream output = new DataOutputStream(con.getOutputStream());

	output.writeBytes(query);

	output.close();

	DataInputStream input = new DataInputStream(con.getInputStream());
	String s = "";
	for (int c = input.read(); c != -1; c = input.read())
		s += String.valueOf((char) c);
	input.close();

	JSONObject json = new JSONObject(s);

	String accesstoken = json.getString("access_token");

	String $urlUserDetails = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token="
			+ accesstoken;

	URL url = new URL($urlUserDetails);
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("GET");
	connection.connect();

	DataInputStream reader = new DataInputStream(connection.getInputStream());

	String userString = "";

	for (int c = reader.read(); c != -1; c = reader.read())
		userString += String.valueOf((char) c);
	reader.close();

	out.println(userString);
	JSONObject userJson = new JSONObject(userString);

	String id = "";
	String name = "";
	String passWord = "";
	String first_name = "";
	String last_name = "";
	String email = "";
	String gender = "";
	String urlPicture = "";
	String type = "google";

	try {
		id = userJson.getString("id");

	} catch (Exception e) {

	}
	try {
		name = userJson.getString("name");
		out.println(name);

	} catch (Exception e) {

	}
	try {

		email = userJson.getString("email");

	} catch (Exception e) {

	}
	try {

		gender = userJson.getString("gender");

		if (gender.equals("male")) {
			gender = "Nam";
		} else if (gender.equals("female")) {
			gender = "Nữ";
		}

	} catch (Exception e) {

	}
	try {
		urlPicture = userJson.getString("picture");

	} catch (Exception e) {

	}

	// Tao user & luu vao db

	User user = new User(0, name,passWord, email, gender, urlPicture, type, id);

	DBConnect connect = new DBConnect();
	DatabaseMetaData dbm = (DatabaseMetaData) connect.getCon().getMetaData();
	// check if "user" table is there
	String table = "user";
	ResultSet tables = dbm.getTables(null, null, table, null);
	if (tables.next()) {
		// Table exists
		// Duyet trong table, neu da ton tai user do, lay thong tin user nay de login // Neu chua thi luu uer moi vao db
		int checkExist = 0;
		String sql = "SELECT * FROM " + table + " WHERE idoftype= '" + id + "' AND type ='google'";
		ResultSet rs = connect.getRs(sql);
		try {

			User userFromDb = new User();
			while (rs.next()) {
				checkExist = 1;

				userFromDb.setEmail(rs.getString("email"));
				userFromDb.setName(rs.getString("name"));
				userFromDb.setGender(rs.getString("gender"));
				userFromDb.seturlPicture(rs.getString("urlPicture"));
				userFromDb.setIdOfType(rs.getString("idoftype"));
				userFromDb.setType(rs.getString("type"));

			}

			if (checkExist == 1) {
				JSONObject jsonToCookie = new JSONObject(userFromDb);
				session.setAttribute("user", jsonToCookie);

				Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
				userCookie.setMaxAge(60 * 60 * 24 * 30);
				userCookie.setPath("/");
				response.addCookie(userCookie);
			} else {
				String command = "INSERT INTO " + table
						+ " ( name , email , gender , urlPicture , type , idoftype) VALUES ( '" + name + "','"
						+ email + "','" + gender + "','" + urlPicture + "','" + type + "','" + id + "')";

				connect.executeUpdate(command);

				// Cooki session

				JSONObject jsonToCookie = new JSONObject(user);

				session.setAttribute("user", jsonToCookie);
				Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
				userCookie.setMaxAge(60 * 60 * 24 * 30);
				userCookie.setPath("/");
				response.addCookie(userCookie);

			}

			//out.println(user.toJsonString());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//out.println(user.toJsonString());

	} else {
		// Table does not exist
		try {
			String command = "CREATE TABLE " + table
					+ " (id int(225) NOT NULL AUTO_INCREMENT, name varchar(1000),passWord varchar(1000), email varchar(1000), gender varchar(1000), urlPicture varchar(5000), type varchar(1000), idoftype varchar(1000), PRIMARY KEY (id) )";
			connect.executeUpdate(command);

		} catch (Exception e) {

		}

		String command = "INSERT INTO " + table
				+ " ( name , email , gender , urlPicture , type , idoftype) VALUES ( '" + name + "','" + email
				+ "','" + gender + "','" + urlPicture + "','" + type + "','" + id + "')";

		connect.executeUpdate(command);

		// Cooki session

		JSONObject jsonToCookie = new JSONObject(user);

		session.setAttribute("user", jsonToCookie);
		Cookie userCookie = new Cookie("user", URLEncoder.encode(jsonToCookie.toString(), "UTF-8"));
		userCookie.setMaxAge(60 * 60 * 24 * 30);
		userCookie.setPath("/");
		response.addCookie(userCookie);

	}
	response.sendRedirect("../../../main.jsp");
%>