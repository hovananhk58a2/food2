package com.food.table;

import java.util.ArrayList;
import java.util.Date;

import javafx.util.Pair;

public class Comment {

	private int userId;
	private ArrayList<Pair<Date, String>> details;

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public ArrayList<Pair<Date, String>> getDetails() {
		return details;
	}

	public void setDetails(ArrayList<Pair<Date, String>> details) {
		this.details = details;
	}

	public Comment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Comment(int userId, ArrayList<Pair<Date, String>> details) {
		super();
		this.userId = userId;
		this.details = details;
	}

	@Override
	public String toString() {
		return "Comment [userId=" + userId + ", details=" + details + "]";
	}

}
