<%@page import="com.food.table.FoodClass"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.food.query.FoodQuery"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="">
<head>
<title>Ăn gì ?</title>
<%@include file="style.jsp"%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

	<jsp:include page="header.jsp"></jsp:include>
	<div class="container">
		<nav>
			<%
				FoodQuery foodQuery = new FoodQuery();
				ArrayList<FoodClass> foodClasses = foodQuery.getFoodClass();
			%>
			<h3>Đồ ăn</h3>
			<ul>
				<%
					for (int i = 0; i < foodClasses.size(); i++) {
				%>
				<li id="classChoser"><a
					href="main.jsp?fclass=<%=foodClasses.get(i).getClassCode()%>&name=<%=foodClasses.get(i).getClassName()%>"><%=foodClasses.get(i).getClassName()%></a></li>
				<%
					}
				%>
			</ul>
			<h3>Sản phẩm bán chạy</h3>
			<ul>
				<li><a href="" target="_blank">Ăn với người yêu</a></li>
				<li><a href="" target="_blank">Góc cafe </a></li>
				<li><a href="" target="_blank">Ăn với gia đình</a></li>
			</ul>
			<h3>Tin nổi bật</h3>
			<ul>
				<li><a href="" target="_blank">Ăn với người yêu</a></li>
				<li><a href="" target="_blank">Góc cafe </a></li>
				<li><a href="" target="_blank">Ăn với gia đình</a></li>
			</ul>
			<h3>Hỗ trợ trực tuyến</h3>
			<ul>
				<li><a href="" target="_blank">Ăn với người yêu</a></li>
				<li><a href="#" target="_blank">Góc cafe </a></li>
				<li><a href="#" target="_blank">Ăn với gia đình</a></li>
			</ul>
		</nav>

		<article id="infoContain">
			<jsp:include page="foodContainer.jsp"></jsp:include>
		</article>

	</div>
	<footer>
		<jsp:include page="footer.jsp"></jsp:include>
	</footer>

	<!-- jQuery -->
	<script src="//code.jquery.com/jquery.js"></script>
	<!-- Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

</body>

</html>