-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2016 at 11:07 AM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web2016`
--

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
CREATE TABLE IF NOT EXISTS `food` (
  `foodcode` int(11) NOT NULL AUTO_INCREMENT,
  `foodname` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `foodclass` json NOT NULL,
  `imageurl` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`foodcode`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`foodcode`, `foodname`, `foodclass`, `imageurl`) VALUES
(1, 'chao suon', '[2, 3, 9, 10, 11, 4]', 'http://lamthenao.me/wp-content/uploads/2016/05/chao-tom-cho-be-an-dam.jpg'),
(2, 'nem chua', '[2, 3, 4, 5, 6, 7]', 'https://blog.kitfe.com/wp-content/uploads/2016/07/nem-chua-TT-2.jpg'),
(3, 'com rang dua bo', '[5, 6, 7, 8, 2, 4]', 'http://naungon.com/images/nau.vn/wp-content/uploads/2014/12/com-rang-dua-bo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `foodclass`
--

DROP TABLE IF EXISTS `foodclass`;
CREATE TABLE IF NOT EXISTS `foodclass` (
  `classcode` int(11) NOT NULL AUTO_INCREMENT,
  `classname` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`classcode`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `foodclass`
--

INSERT INTO `foodclass` (`classcode`, `classname`) VALUES
(1, 'Tất cả'),
(2, 'Ăn với người yêu'),
(3, 'Góc cà phê'),
(4, 'Ăn với gia đình'),
(5, 'Ăn với nhóm'),
(6, 'Tiệm bánh'),
(7, 'Hải sản'),
(8, 'Nướng & Lẩu'),
(9, 'Nhật Bản'),
(10, 'Pizza'),
(11, 'Ăn vặt & Vỉa hè'),
(12, 'Ăn một mình'),
(13, 'Hàn Quốc'),
(14, ''),
(123, 'Tiếng Việt');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(225) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `passWord` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `urlPicture` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idoftype` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `passWord`, `email`, `gender`, `urlPicture`, `type`, `idoftype`) VALUES
(17, 'Ngoc', 'cd0aa9856147b6c5b4ff2b7dfee5da20aa38253099ef1b4a64aced233c9afe29', '111@gmail.com', 'Nam', 'eset/image/avatar/default_avatar.jpg', 'normal', ''),
(18, 'Thanh123', 'b8dc2c143be8994682b08461f46487e05874e59dd9ab65cf973e3a3c67a763aa', 'thanh@gmail.com', 'Nam', 'eset/image/avatar/default_avatar.jpg', 'normal', ''),
(16, 'ThanhPV', '4e07408562bedb8b60ce05c1decfe3ad16b72230967de01f640b7e4729b49fce', '1@gmail.com', 'Khác', 'eset/image/avatar/default_avatar.jpg', 'normal', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
