package com.food.user;

public class User {

	private int id = 0;
	private String name = "";
	private String passWord = "";
	private String email = "";
	private String gender = "";
	private String urlPicture = "";
	private String type = ""; // admin, normal, facebook, google.
	private String idoftype = ""; // for facebook or google

	public User() {
	}

	public User(int id, String name, String passWord, String email, String gender, String urlPicture, String type, String idoftype) {
		this.id = id;
		this.email = email;
		this.passWord = passWord;
		this.gender = gender;
		this.name = name;
		this.urlPicture = urlPicture;
		this.type = type;
		this.idoftype = idoftype;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIdoftype() {
		return idoftype;
	}

	public void setIdoftype(String idoftype) {
		this.idoftype = idoftype;
	}

	public void setUrlPicture(String urlPicture) {
		this.urlPicture = urlPicture;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void seturlPicture(String url) {
		this.urlPicture = url;
	}
	
	public void setIdOfType(String id) {
		this.idoftype = id;
	}
	
	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return this.name;
	}
	
	public String getPassWord() {
		return this.passWord;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getGender() {
		return this.gender;
	}
	
	public String getUrlPicture() {
		return this.urlPicture;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getIdOfType() {
		return this.idoftype;
	}

	public String toJsonString() {
		String s = "{\"id\":\"" + idoftype + "\",\"name\":\"" + name + "\",\"email\":\"" + email + "\",\"picture\":\""
				+ urlPicture + "\",\"gender\":\"" + gender + "\"}";
		return s;
	}

}
